#!/usr/bin/env bash

# Colors
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
RED='\033[0;31m'
CYAN='\033[0;36m'
NC='\033[0m'

# Emojis
CLONE_EMOJI="📥"
START_EMOJI="🚀"
SUCCESS_EMOJI="✅"
ERROR_EMOJI="❌"
LOADING_EMOJI=("⠋" "⠙" "⠹" "⠸" "⠼" "⠴" "⠦" "⠧" "⠇" "⠏")

clone_if_not_exists() {
  local repo_name=$1
  local repo_url=$2
  local target_dir=$3

  local project_dir="$target_dir/$repo_name"

  if [ -d "$project_dir" ]; then
    echo -e "${GREEN}${CLONE_EMOJI} Directory $project_dir already exists.${NC}"
  else
    git clone "$repo_url" "$project_dir"
    echo -e "${GREEN}${CLONE_EMOJI} Repository $repo_name cloned to $project_dir.${NC}"
  fi
}

start_runner() {
  local compose_file=$1
  local project_name=$2
  local service_name=$3
  echo $compose_file
  echo $project_name
  echo $service_name
  echo -e "\n${YELLOW}Starting runner:${NC}"
  if ! docker-compose -f "$compose_file" -p "$project_name" up -d --build "$service_name"; then
    echo -e "${RED}${ERROR_EMOJI} Failed to start runner.${NC}"
    exit 1
  else
    echo -e "\n${GREEN}${SUCCESS_EMOJI} Runner started.${NC}"
  fi
}

generate_random_tags() {
  random_tags=$(openssl rand -base64 4 | tr -dc 'a-zA-Z0-9' | head -c 6)
  echo "$random_tags"
}

update_tags() {
  local repository=$1
  local file_path=$2
  local new_tag=$3

  curl "https://gitlab.com/chahirchalouati1/${repository}/-/raw/main/.gitlab-ci.yml?inline=false" | sed "s/\$new_tag/$new_tag/g"  >"$file_path"

  echo "Updated tags in gitlab-ci.yaml: $file_path"
}
