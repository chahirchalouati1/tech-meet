#!/bin/bash

# GitLab API endpoint
API_URL="https://gitlab.com/api/v4"
# Personal access token with appropriate permissions
# ACCESS_TOKEN="glpat-Lcm-4HggsoFYkS4vjDtm"à

ACCESS_TOKEN="glpat-9QSyPgjsgnzkUxoWyt-y"
# GitLab project ID
#PROJECT_ID="46113280"
PROJECT_ID="47374872"

# Step 1: Get list of runner IDs
RUNNER_IDS=$(curl --header "Authorization: Bearer $ACCESS_TOKEN" --request GET "$API_URL/runners" | jq -r '.[].id')

# Step 2: Iterate over runner IDs and delete them
for RUNNER_ID in $RUNNER_IDS
do
    echo "Deleting runner with ID: $RUNNER_ID"
    curl --header "Authorization: Bearer $ACCESS_TOKEN" --request DELETE "$API_URL/runners/$RUNNER_ID"
done